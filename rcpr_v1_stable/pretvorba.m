% fName = 'novost';
% info.codec = '.jpg';
% info.fps = 16;
% varargin.Is = ['001.jpg', '002.jpg', '003.jpg', '004.jpg','005.jpg', '006.jpg', '007.jpg', '008.jpg', '009.jpg', '010.jpg', '011.jpg', '012.jpg', '013.jpg'];
% varargin.sDir = 'C:\Users\Damjan\Documents\MATLAB\images';
% seqIo(fName, 'frImgs', info, varargin)

 fName = 'video-4';
 info.codec = 'jpg';
 varargin.aviName = 'video-4.avi';
 varargin.sDir = 'C:\\Users\\Damjan\\Documents\\MATLAB\\rcpr_v1_stable\\';
 seqIo( fName, 'frImgs', info, varargin)

% INPUTS
%  fName      - seq file name
%  info       - defines codec, etc, see seqIo>writer
%  varargin   - additional params (struct or name/value pairs)
%   .aviName    - [] if specified create seq from avi file
%   .Is         - [] if specified create seq from image array
%   .sDir       - [] source directory
%   .skip       - [1] skip between frames
%   .name       - ['I'] base name of images
%   .nDigits    - [5] number of digits for filename index
%   .f0         - [0] first frame to read
%   .f1         - [10^6] last frame to read