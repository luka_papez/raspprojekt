import numpy as np  
import cv2  
import dlib  
import sys

detectEvery = 3
pointsInTrajectory = 5

if len(sys.argv) != 3:
  print "------"
  print "Usage: python <program>.py detect_every trajectory_length"
  print "Now running with defaults, detect_every:", detectEvery, "trajectory_length:", pointsInTrajectory
  print "------"

else:
  detectEvery = int(sys.argv[1])
  pointsInTrajectory = int(sys.argv[2])
  
  
cascade_path = "haarcascade_frontalface_default.xml"  
predictor_path= "shape_predictor_68_face_landmarks.dat"  
 
# Create the haar cascade  
faceCascade = cv2.CascadeClassifier(cascade_path)  
 
# create the landmark predictor  
predictor = dlib.shape_predictor(predictor_path)  

video_capture = cv2.VideoCapture(0)

# try to wait a bit for the camera
if not video_capture.isOpened():
    cv2.waitKey(1000)
  
if not video_capture.isOpened():
    print "Couldn't open camera"
else:
    print "Camera opened"

faces = None
iteration = 0

trajectory = []
currentPoints = 0
drawTrajectory = True

while(True):

    # Read the image  
    ret, image = video_capture.read()
    # convert the image to grayscale  
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)  

    # Detect faces in the image  
    if iteration % detectEvery == 0:
      faces = faceCascade.detectMultiScale(  
        gray,  
        scaleFactor=1.05,  
        minNeighbors=5,  
        minSize=(100, 100),  
        flags=0
      )
      """
      # if a single face is found add it to trajectory
      if len(faces) == 1:
        x, y, w, h = faces[0]
        curr = (x + w / 2, y + h / 2)
        if currentPoints >= pointsInTrajectory:
          trajectory[currentPoints % pointsInTrajectory] = curr
          
        else:
          trajectory.append(curr)
        
        currentPoints = currentPoints + 1
    """
        

    print "Found {0} faces!".format(len(faces))  

    # Draw a rectangle around the faces  
    for (x, y, w, h) in faces:  
        print x, y, w, h
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)  

        # Converting the OpenCV rectangle coordinates to Dlib rectangle  
        dlib_rect = dlib.rectangle(int(x), int(y), int(x + w), int(y + h))
        print dlib_rect  

        detected_landmarks = predictor(image, dlib_rect).parts()  

        landmarks = np.matrix([[p.x, p.y] for p in detected_landmarks])

        if len(faces) != 1:
            drawTrajectory = False
            trajectory = []
        else:
            drawTrajectory = True

        for idx, point in enumerate(landmarks):  
            pos = (point[0, 0], point[0, 1])  

            # add point to trajectory
            if drawTrajectory:
                # create additional lists if needed
                while idx >= len(trajectory):
                    trajectory.append([])

                trajectory[idx].append(pos)

                # remove oldest points in trajectory if needed
                while len(trajectory[idx]) > pointsInTrajectory:
                    trajectory[idx].pop(0)

            # draw points on the landmark positions
            cv2.circle(image, pos, 3, color=(0, 255, 255))  

            # annotate the positions  
            """
            cv2.putText(image, str(idx), pos,  
               fontFace=cv2.FONT_HERSHEY_SIMPLEX,  
               fontScale=0.4,  
               color=(0, 0, 255))  
            """
       
    # draw trajectory
    if drawTrajectory:
        for traj in trajectory:
            for i in range(1, len(traj)):
                cv2.line(image, traj[i - 1], traj[i], color = (0, 0, 255), thickness = 1)      

       
    cv2.imshow("Faces found", image)  
    k = cv2.waitKey(5)  
    
    # disable trajectory drawing if key pressed
    if k != -1:
      drawTrajectory = not drawTrajectory
    
    iteration = iteration + 1
  
  
