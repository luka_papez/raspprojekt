import numpy as np  
import cv2  
import dlib  

# TODO:
# 1) interpolate colour between edges so that the transition looks smoother
# 2) include more face (eyebrows, forehead)

# loading data

cascade_path = "haarcascade_frontalface_default.xml"  
predictor_path= "shape_predictor_68_face_landmarks.dat"  
 
# Create the haar cascade  
faceCascade = cv2.CascadeClassifier(cascade_path)  
 
# create the landmark predictor  
predictor = dlib.shape_predictor(predictor_path)  

video_capture = cv2.VideoCapture(0)

# try to wait a bit for the camera
if not video_capture.isOpened():
    cv2.waitKey(1000)
  
if not video_capture.isOpened():
    print "Couldn't open camera"
else:
    print "Camera opened"

##################################
# preparing substituting face

loaded_image = cv2.imread("mamic.jpg")
#loaded_image = cv2.imread("arnold.jpg")

gray = cv2.cvtColor(loaded_image, cv2.COLOR_BGR2GRAY)  

# Detect faces in the image  
faces = faceCascade.detectMultiScale(  
    gray,  
    scaleFactor=1.05,  
    minNeighbors=5,  
    minSize=(100, 100),  
    flags=0
)

x, y, w, h = faces[0]
roi = loaded_image[y:(y + h), x:(x + w)]

# Converting the OpenCV rectangle coordinates to Dlib rectangle  
dlib_rect = dlib.rectangle(int(x), int(y), int(x + w), int(y + h))

detected_landmarks = predictor(loaded_image, dlib_rect).parts()  

source_points = ([(p.x, p.y) for p in detected_landmarks])

mask = np.zeros(loaded_image.shape, dtype=np.uint8)

# to understand this you need to look at how DLib numerates the points of the face
# the idea is to just take the outline of the face
corners = source_points[0:16] + list(reversed(source_points[22:27])) + list(reversed(source_points[17:22]))

source_points = np.array(source_points)

roi_corners = np.array([corners], dtype=np.int32)
# [[(10,10), (300,300), (10,300)]]
# fill the ROI so it doesn't get wiped out when the mask is applied
channel_count = loaded_image.shape[2]  # i.e. 3 or 4 depending on your image
ignore_mask_color = (255,)*channel_count
cv2.fillPoly(mask, roi_corners, ignore_mask_color)

# apply the mask
masked_image = cv2.bitwise_and(loaded_image, mask)

while(True):

    # Read the image  
    ret, image = video_capture.read()
    # convert the image to grayscale  
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)  

    # Detect faces in the image  
    faces = faceCascade.detectMultiScale(  
        gray,  
        scaleFactor=1.05,  
        minNeighbors=5,  
        minSize=(100, 100),  
        flags=0
    ) 

    print "Found {0} faces!".format(len(faces))     
    
    # Draw a rectangle around the faces  
    for (x, y, w, h) in faces:  

        # Converting the OpenCV rectangle coordinates to Dlib rectangle  
        dlib_rect = dlib.rectangle(int(x), int(y), int(x + w), int(y + h))

        detected_landmarks = predictor(image, dlib_rect).parts()  

        landmarks = np.matrix([[p.x, p.y] for p in detected_landmarks]) 
        
        dest_points = np.array([(p.x, p.y) for p in detected_landmarks])
        
        # Calculate Homography
        h, status = cv2.findHomography(source_points, dest_points)
         
        # Warp source image to destination based on homography
        im_out = cv2.warpPerspective(masked_image, h, (image.shape[1], image.shape[0]))
        
        # create a mask removing your face
        ret, face_mask = cv2.threshold(im_out, 1, 255, cv2.THRESH_BINARY_INV)
        
        # remove your face from the image
        image_2 = cv2.bitwise_and(image, face_mask)
        
        # add substituted face to yours
        image = cv2.add(image_2, im_out)
    
    cv2.imshow("Faces found", image)  
    cv2.waitKey(5)  
  
  
